package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.elements.Flower;
import com.epam.rd.java.basic.task8.elements.GrowingTips;
import com.epam.rd.java.basic.task8.elements.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
    String inerData;
	List<Flower> flowers;
	Flower curentFlower;
	VisualParameters curentVP;
	GrowingTips curentGT;
	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}


	public List<Flower> getResult() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(new File(xmlFileName), this);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return flowers;
	}

	@Override
	public void startDocument() throws SAXException {
		flowers=new ArrayList<>();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("flower"))
			curentFlower=new Flower();

		//measure
        if (qName.equalsIgnoreCase("aveLenFlower"))
			curentVP.measure=attributes.getValue("measure");
		if (qName.equalsIgnoreCase("watering"))
			curentGT.measureW=attributes.getValue("measure");
		if (qName.equalsIgnoreCase("lighting"))
			curentGT.lighting=attributes.getValue("lightRequiring").equalsIgnoreCase("yes");
		if (qName.equalsIgnoreCase("tempreture"))
			curentGT.measureT=attributes.getValue("measure");


		if (qName.equalsIgnoreCase("visualParameters"))
			curentVP=new VisualParameters();
		if (qName.equalsIgnoreCase("growingTips"))
			curentGT=new GrowingTips();
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		//add inner value
		switch (qName){
			case "name": curentFlower.name=inerData;break;
			case"soil": curentFlower.soil=inerData;break;
			case"origin":curentFlower.origin=inerData;break;
			case "stemColour": curentVP.stemColour=inerData;break;
			case"leafColour": curentVP.leafColour=inerData;break;
			//case "lighting": curentGT.lighting=inerData.equalsIgnoreCase("yes");break;
			case"watering": curentGT.watering= Integer.parseInt(inerData); break;
			case"tempreture": curentGT.tempreture= Integer.parseInt(inerData); break;
			case"aveLenFlower": curentVP.aveLenFlower= Integer.parseInt(inerData); break;
			case "multiplying": curentFlower.multiplying=inerData;break;
		}

		if (qName.equalsIgnoreCase("visualParameters"))
			curentFlower.vp=curentVP;
		if (qName.equalsIgnoreCase("growingTips"))
			curentFlower.gt=curentGT;
		if (qName.equalsIgnoreCase("flower"))
			flowers.add(curentFlower);
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		inerData=new String(ch,start,length);
	}
}