package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.elements.Flower;
import com.epam.rd.java.basic.task8.elements.GrowingTips;
import com.epam.rd.java.basic.task8.elements.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	XMLEventReader reader;

	List<Flower> flowers;
	Flower curentFlower;
	VisualParameters curentVP;
	GrowingTips curentGT;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		XMLInputFactory factory= XMLInputFactory.newInstance();
		try {
			reader = factory.createXMLEventReader(new FileInputStream(xmlFileName));
		}catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<Flower> getResult() {
		flowers=new ArrayList<>();
		try {
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()) {
						case "flower":
							curentFlower = new Flower();
							break;
						case "name":
							nextEvent = reader.nextEvent();
							curentFlower.name=nextEvent.asCharacters().getData();
							break;
						case "soil":
							nextEvent = reader.nextEvent();
							curentFlower.soil=nextEvent.asCharacters().getData();
							break;
						case "origin":
							nextEvent = reader.nextEvent();
							curentFlower.origin=nextEvent.asCharacters().getData();
							break;
						case "multiplying":
							nextEvent = reader.nextEvent();
							curentFlower.multiplying=nextEvent.asCharacters().getData();
							break;
						case "stemColour":
							nextEvent = reader.nextEvent();
							curentVP.stemColour=nextEvent.asCharacters().getData();
							break;
						case "leafColour":
						nextEvent = reader.nextEvent();
						curentVP.leafColour=nextEvent.asCharacters().getData();
						break;

						case "aveLenFlower":
							nextEvent = reader.nextEvent();
							curentVP.aveLenFlower= Integer.parseInt(nextEvent.asCharacters().getData());
							curentVP.measure=startElement.getAttributeByName(new QName("measure")).getValue();
							break;

						case "lighting":
							nextEvent = reader.nextEvent();
							curentGT.lighting=startElement.getAttributeByName(new QName("lightRequiring")).getValue().equalsIgnoreCase("yes");
							break;
						case "tempreture":
							nextEvent = reader.nextEvent();
							curentGT.tempreture= Integer.parseInt(nextEvent.asCharacters().getData());
							curentGT.measureT=startElement.getAttributeByName(new QName("measure")).getValue();
							break;
						case "watering":
							nextEvent = reader.nextEvent();
							curentGT.watering= Integer.parseInt(nextEvent.asCharacters().getData());
							curentGT.measureW=startElement.getAttributeByName(new QName("measure")).getValue();
							break;

						case "visualParameters":
							curentVP=new VisualParameters();
							break;
						case "growingTips":
							curentGT=new GrowingTips();
							break;
					}
				}
				if (nextEvent.isEndElement()) {
					EndElement endElement = nextEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						curentFlower.gt=curentGT;
						curentFlower.vp=curentVP;
						flowers.add(curentFlower);
					}
				}
			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		return flowers;
	}


}