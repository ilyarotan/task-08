package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.elements.Flower;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static <fin> void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		File solution = new File(xmlFileName);




		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parse();
		List<Flower> flowers= domController.getResult();

		// sort (case 1)

		Collections.sort(flowers, Comparator.comparing(Flower::getName));

		// save
		String outputXmlFile = "output.dom.xml";
		String out="<flowers xmlns=\"http://www.nure.ua\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n";
		for (Flower flower:flowers)
			out+=flower.toString();
		out+="</flowers>";

		try(FileWriter writer = new FileWriter(outputXmlFile, false))
		{

			writer.write(out);
			writer.flush();
		}
		catch(IOException ex){
			System.out.println(ex.getMessage());
		}

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		List<Flower> flowersSAX= saxController.getResult();


		// sort  (case 2)
		Collections.sort(flowersSAX, Comparator.comparing(Flower::getName));

		// save
		out="<flowers xmlns=\"http://www.nure.ua\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n";
		for (Flower flower:flowersSAX)
			out+=flower.toString();
		out+="</flowers>";
		outputXmlFile = "output.sax.xml";

		try(FileWriter writer = new FileWriter(outputXmlFile, false))
		{

			writer.write(out);
			writer.flush();
		}
		catch(IOException ex){
			System.out.println(ex.getMessage());
		}


		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> flowersSTAX= staxController.getResult();

		// save
		outputXmlFile = "output.stax.xml";
		out="<flowers xmlns=\"http://www.nure.ua\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n";
		for (Flower flower:flowersSTAX)
			out+=flower.toString();
		out+="</flowers>";

		try(FileWriter writer = new FileWriter(outputXmlFile, false))
		{

			writer.write(out);
			writer.flush();
		}
		catch(IOException ex) {
			System.out.println(ex.getMessage());
		}
	}


}
