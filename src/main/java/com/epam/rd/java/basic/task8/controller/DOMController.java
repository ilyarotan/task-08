package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.elements.Flower;
import com.epam.rd.java.basic.task8.elements.GrowingTips;
import com.epam.rd.java.basic.task8.elements.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	Document document;
	List<Flower> flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();


	public Document parse(){
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
			document= builder.parse(new File(xmlFileName));
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return document;
	}


	public List<Flower> getResult() {
		flowers=new ArrayList<>();
		NodeList flowerElements=document.getDocumentElement().getElementsByTagName("flower");
		for (int i = 0; i < flowerElements.getLength(); i++) {
			Flower flower=new Flower();
			VisualParameters vp=new VisualParameters();
			GrowingTips gt=new GrowingTips();
			NodeList flowerNode = flowerElements.item(i).getChildNodes();

		    int k=0;
			while (flowerNode.item(k).getNodeType() == Node.TEXT_NODE) k++;
			flower.name=flowerNode.item(k).getTextContent();
			k++;while (flowerNode.item(k).getNodeType() == Node.TEXT_NODE) k++;
			flower.soil=flowerNode.item(k).getTextContent();
			k++;while (flowerNode.item(k).getNodeType() == Node.TEXT_NODE) k++;
			flower.origin=flowerNode.item(k).getTextContent();
			k++;while (flowerNode.item(k).getNodeType() == Node.TEXT_NODE) k++;

			NodeList vpNode = flowerNode.item(k).getChildNodes();
			int j=0;
			while (vpNode.item(j).getNodeType() == Node.TEXT_NODE) j++;
			vp.stemColour=vpNode.item(j).getTextContent();
			j++;while (vpNode.item(j).getNodeType() == Node.TEXT_NODE) j++;
			vp.leafColour=vpNode.item(j).getTextContent();
			j++;while (vpNode.item(j).getNodeType() == Node.TEXT_NODE) j++;
			vp.aveLenFlower=Integer.parseInt(vpNode.item(j).getTextContent());
			vp.measure=vpNode.item(j).getAttributes().getNamedItem("measure").getNodeValue();
			k++;while (flowerNode.item(k).getNodeType() == Node.TEXT_NODE) k++;

			j=0;
			NodeList gtNode = flowerNode.item(k).getChildNodes();
			while (vpNode.item(j).getNodeType() == Node.TEXT_NODE) j++;
			gt.tempreture= Integer.parseInt(gtNode.item(j).getTextContent());
			gt.measureT=gtNode.item(j).getAttributes().getNamedItem("measure").getNodeValue();
			j++;while (vpNode.item(j).getNodeType() == Node.TEXT_NODE) j++;
			gt.lighting=gtNode.item(j).getAttributes()
					.getNamedItem("lightRequiring").getNodeValue().equalsIgnoreCase("yes");
			j++;while (vpNode.item(j).getNodeType() == Node.TEXT_NODE) j++;
			gt.watering= Integer.parseInt(gtNode.item(j).getTextContent());
			gt.measureW=gtNode.item(j).getAttributes().getNamedItem("measure").getNodeValue();
			k++;while (flowerNode.item(k).getNodeType() == Node.TEXT_NODE) k++;
			flower.multiplying=flowerNode.item(k).getTextContent();

			flower.vp=vp;
			flower.gt=gt;
			flowers.add(flower);
		}
		return flowers;
	}
}
